# voxl-microdds-agent

This service provides a microdds agent running on port 8888 on localhost to communicate with a client over the ros2 microdds bridge.

This is a build of eProsima Micro XRCE-DDS v2.4.1 built for QRB5165 using the native compiler.

Once installed enable the service with

```
voxl-configure-microdds enable
```

That step will automatically be done with the voxl-configure-mpa-to-ros2 script though so it shouldn't be necessary.


## Build dependencies

* NONE

This README covers building this package.


## Build Instructions

0) Update submodule outside of docker

```bash
git submodule update --init --recursive
```

1) Launch the qrb5165-emulator v1.5 docker

```bash
~/git/voxl-microdds-agent$ voxl-docker -i qrb5165-emulator:1.5
qrb5165-emulator:~$
```

2) Install dependencies inside the docker. You must specify both the hardware platform and binary repo section to pull from. CI will use the `dev` binary repo for `dev` branch jobs, otherwise it will select the correct target SDK-release based on tags. When building yourself, you must decide what your intended target is, usually `dev` or `staging`

```bash
qrb5165-emulator:~$ ./install_build_deps.sh qrb5165 staging
```


3) Build scripts should take the hardware platform as an argument: `qrb5165`. CI will pass these arguments to the build script based on the job target.

```bash
qrb5165-emulator:~$ ./build.sh qrb5165
```


4) Make an deb package while still inside the docker.

```bash
qrb5165-emulator:~$ ./make_package.sh deb
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.


## Deploy to VOXL

You can now push the deb package to VOXL and install with dpkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed.

```bash
(outside of docker)
voxl-microdds-agent$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
voxl-microdds-agent$ ./deploy_to_voxl.sh ssh 192.168.1.123
```

