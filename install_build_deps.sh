#!/bin/bash

# list all your dependencies here
## this list is just for tab-completion
## it's not an exhaustive list of platforms available.
AVAILABLE_PLATFORMS="qrb5165"


print_usage(){
    echo ""
    echo " Install build dependencies from a specified repository."
    echo ""
    echo " Usage:"
    echo "  ./install_build_deps.sh {platform} {section}"
    echo ""
    echo " Examples:"
    echo ""
    echo "  ./install_build_deps.sh qrb5165 dev"
    echo "        Install from qrb5165 development repo."
    echo ""
    echo "  ./install_build_deps.sh qrb5165 sdk-1.0"
    echo "        Install from qrb5165 sdk-1.0 repo."
    echo ""
    echo ""
    echo " These examples are not an exhaustive list."
    echo " Any platform and section in this deb repo can be used:"
    echo "     http://voxl-packages.modalai.com/dists/"
    echo ""
}

# make sure two arguments were given
if [ "$#" -ne 2 ]; then
    print_usage
    exit 1
fi
MODE="DEB"

## convert arguments to lower case for robustness
PLATFORM=$(echo "$1" | tr '[:upper:]' '[:lower:]')
SECTION=$(echo "$2" | tr '[:upper:]' '[:lower:]')

# install deb packages with apt
echo "using $PLATFORM $SECTION debian repo"
# write in the new entry
DPKG_FILE="/etc/apt/sources.list.d/modalai.list"
LINE="deb [trusted=yes] http://voxl-packages.modalai.com/ ./dists/$PLATFORM/$SECTION/binary-arm64/"
sudo echo "${LINE}" > ${DPKG_FILE}

# ## make sure we have the latest package index
# ## only pull from voxl-packages to save time
# sudo apt-get update -o Dir::Etc::sourcelist="sources.list.d/modalai.list" -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"

# ## install the user's list of dependencies
# echo "installing: $DEPS_QRB5165"
# sudo apt install -y $DEPS_QRB5165

sudo apt-get update
sudo apt-get install ca-certificates gpg wget

# Define the required CMake version
required_version="3.16"

# Get the installed CMake version
installed_version=$(cmake --version | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")

# Compare the installed version with the required version
if [[ $(printf "%s\n" "$required_version" "$installed_version" | sort -V | head -n 1) == "$required_version" ]]; then
    echo "CMake version $required_version or above is installed."
else
    #sudo apt remove -y cmake
    echo "================================"
    echo "======= Upgrading CMAKE ========"
    echo "================================"
    sudo apt-get update
    echo "installing software-properties-common"
    sudo apt-get install -y software-properties-common
    echo "disabling ipv6 in gnupg"
    mkdir -p ~/.gnupg
    echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf

    # manually obtain copy of keyring
    test -f /usr/share/doc/kitware-archive-keyring/copyright || wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null

    echo "adding kitware repository to sources"
    echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null
    sudo apt-get update

    sudo apt-get install kitware-archive-keyring

    echo "running apt-get install -y cmake to hopefully get the newest version"
    sudo apt-get install -y cmake
fi


echo ""
echo "Done installing dependencies"
echo ""
exit 0
