#!/bin/bash

## voxl-cross contains the following toolchains
## last for qrb5165
TOOLCHAIN_QRB5165="/opt/cross_toolchain/aarch64-gnu-7.toolchain.cmake"

# placeholder in case more cmake opts need to be added later
EXTRA_OPTS=""

## this list is just for tab-completion
AVAILABLE_PLATFORMS="qrb5165"


print_usage(){
	echo ""
	echo " Build the current project based on platform target."
	echo ""
	echo " Usage:"
	echo ""
	echo "  ./build.sh qrb5165"
	echo "        Build 64-bit binaries for qrb5165"
	echo ""
	echo ""
}


case "$1" in
	qrb5165)
		cd Micro-XRCE-DDS-Agent
		mkdir -p build
		cd build
		cmake ..
		make -j$(nproc)
		;;
	*)
		print_usage
		exit 1
		;;
esac



